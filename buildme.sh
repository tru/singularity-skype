#!/bin/sh
# I assume that singularity is installed as /usr/bin/singularity
# and the singularity containers images are in ~/singularity-img
PATH=/usr/bin:/usr/sbin:/bin
if [ -f /usr/bin/singularity ] && [ ! -f ~/singularity-img/skype.img ] && [ -f singularity-debian-amd64-stretch-fr-skype.def ]; then
sudo singularity create --size 2000 ~/singularity-img/skype.img  && \
sudo singularity bootstrap ~/singularity-img/skype.img singularity-debian-amd64-stretch-fr-skype.def && \
echo  ~/singularity-img/skype.img built
fi
