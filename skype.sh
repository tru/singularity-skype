#!/bin/sh
d=$HOME/.singularity/skype
S=~/singularity-img/skype.img
echo "running persistant skype in $d"
if [ -d $d ]; then
singularity exec -B /run -H $d $S /usr/bin/skypeforlinux "$@"
fi
