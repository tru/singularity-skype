#!/bin/sh
d=`mktemp -d /dev/shm/skypeXXXX`
S=~/singularity-img/skype.img
echo "running non persistant skype in $d"
if [ -d $d ]; then
singularity exec -B /run -H $d $S /usr/bin/skypeforlinux "$@"
echo "removing $d"
/bin/rm -rf $d
fi
