I am running a CentOS-7 laptop and I want to have skype running but as fo 2017/07/19:

https://www.skype.com/en/download-skype/skype-for-linux/
```
Important notice: All Skype for Linux clients version 4.3 and older will be
retired on July 1, 2017. To keep chatting, please install the latest version of
Skype for Linux.
```
Skype for Linux System Requirements
```
64-bit Ubuntu 14.04+, 64-bit Debian 8+, 64-bit openSUSE 13.3+, or 64-bit Fedora Linux 24+
...
```

1. install singularity
2. build the container with
  * sudo singularity create --size 2000 skype.img && sudo singularity bootstrap skype.img singularity-debian-amd64-stretch-fr-skype.def
  * or run ./buildme.sh
3. use/adapt either skype.sh or skype-private.sh  and use them
